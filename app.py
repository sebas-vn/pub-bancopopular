from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
import datetime 

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = 'mysql+pymysql://usermaia:1qa2ws3E.2017*@104.131.169.113/banco_popular'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

db = SQLAlchemy(app)

# Model of table Alfa 
class Alfa(db.Model):
  user_name = db.Column(db.String(60), nullable=False)
  phone = db.Column(db.String(20), nullable=False, primary_key=True)
  auth = db.Column(db.String(20))
  area = db.Column(db.String(20))
  genero = db.Column(db.String(10))
  cedula = db.Column(db.String(15))

  def __repr__(self):
    return '<Alfa %r>' % self.phone

# Model of table interactions
class Interactions(db.Model):
  id_interaction = db.Column(db.Integer, primary_key=True)
  user = db.Column(db.String(20), nullable=False)
  question = db.Column(db.Integer, nullable=False)
  date = db.Column(db.DateTime)
  status = db.Column(db.String(45))
  diagnostics = db.Column(db.Integer)
  answer = db.Column(db.String(5))

  def __repr__(self, user, question, date, status, diagnostics, answer):
    self.user = user
    self.question = question
    self.date = date
    self.status = status
    self.diagnostics = diagnostics
    self.answer = Answer

# Route to insert data in interactions table
@app.route("/post-interaction", methods=['GET', 'POST'])
def addInteraction():
  user = request.form.get("user")
  question = request.form.get("question")
  date = datetime.datetime.now()
  status = request.form.get("status")
  diagnostics = request.form.get("diagnostics")
  answer = request.form.get("answer")

  interaction = Interactions(user=user, question=question, date=date, status=status, diagnostics=diagnostics, answer=answer)
  db.session.add(interaction)
  db.session.commit()
  return 'Executed'

# Route to query and authenticate if user exists 
@app.route("/auth", methods=['GET', 'POST'])
def auth():
  phone = request.form.get("phone")

  user = Alfa.query.filter_by(phone=phone).first()
  if (user is None):
    return '0'
  else:
    return '1'
  


if __name__ == "__main__":
  app.run(host="0.0.0.0", port=5014, debug=False)
